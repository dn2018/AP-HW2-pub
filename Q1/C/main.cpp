#include<iostream>

int main(){
  char a{'a'};
  const char* name{"Soroush Dehghan"};
  const char* p1{name};//pointer
  std::cout << *p1 << *(p1 + 1) << *(p1 + 2) << std::endl;
  p1 = &a; //Allowed = yes
  std::cout << *p1 << *(p1 + 1) << *(p1 + 2) << std::endl;
  p1 = name;
  *p1 = 'b'; //Allowed = no
  return 0;
}
