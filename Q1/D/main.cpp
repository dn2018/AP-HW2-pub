#include<iostream>

int main(){
  int* p1{new int[10]};//array with length 10
  int* p2[10];//pointer to int in adrress 10
  std::cout << p2 << "," << *p2 << "," << *(p2+1) ;
  int (*p3)(int[]);
  int (*p4[10])(int [][10]);
  int (*p5)[10]{new int [10][10]};//int (*p5)[10]{}; p5 = new int [10][10];
  return 0;
}
